package com.beginners.curemanager.analytics

import android.os.Bundle

class FirebaseAnalyticsParamBuilder {
    private val bundle = Bundle()

    fun putParam(
        key: FirebaseEvent.Key,
        value: FirebaseEvent.Value
    ): FirebaseAnalyticsParamBuilder {
        bundle.putString(key.value, value.name)
        return this
    }

    fun build(): Bundle {
        return bundle
    }
}