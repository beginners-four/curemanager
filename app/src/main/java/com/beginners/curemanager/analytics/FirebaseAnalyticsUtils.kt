package com.beginners.curemanager.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

object FirebaseAnalyticsUtils {
    private var firebaseAnalytics: FirebaseAnalytics? = null

    fun sendEvent(context: Context, name: FirebaseEvent.Name, bundle: Bundle? = null) {
        getFirebaseAnalytics(context).logEvent(name.value, bundle)
    }

    private fun getFirebaseAnalytics(context: Context): FirebaseAnalytics {
        if (firebaseAnalytics == null) {
            firebaseAnalytics = FirebaseAnalytics.getInstance(context)
        }
        return firebaseAnalytics!!
    }

    /**
     * リマインダーON/OFFのユーザープロパティを送信
     */
    fun sendUserPropertyExerciseReminder(context: Context, isOn: Boolean) {
        getFirebaseAnalytics(context).setUserProperty(
            FirebaseUserProperty.EXERCISE_REMINDER.value,
            isOn.toString()
        )
    }
}