package com.beginners.curemanager.analytics

import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseEvent {
    enum class Name constructor(val value: String) {
        TUTORIAL_BEGIN(FirebaseAnalytics.Event.TUTORIAL_BEGIN),
        TUTORIAL_COMPLETE(FirebaseAnalytics.Event.TUTORIAL_COMPLETE),
        HOME_EXERCISE_BUTTON("home_exercise_button"),
        SELECT_CURE_POINT("select_cure_point_edit"),
        PRE_EXERCISE("pre_exercise"),
        EXERCISE("exercise"),
        RECORD("record"),
        SETTING("setting"),
    }

    enum class Key constructor(val value: String) {
        TYPE("type"),
        ACTION("action"),
        FROM("from"),
        TITLE("title")
    }

    enum class Value {
        show,
        click,
        setting_on,
        setting_off,
        start,
        acupoint,
        stretch,
        aerobic,
        body_make,
        suspend,
        early_finish,
        complete,
        reminder,
        about,
        term,
        policy,
        inquiry,
        sigin_in,
        sigin_out,
    }
}