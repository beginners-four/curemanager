package com.beginners.curemanager.analytics

enum class FirebaseUserProperty constructor(val value:String){
    EXERCISE_REMINDER("exercise_reminder"),
}