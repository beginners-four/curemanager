package com.beginners.curemanager.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object CommonRetrofitService {
    private val client = OkHttpClient.Builder().build()
    private const val baseUrl = "https://asia-northeast1-cure-manager-dev.cloudfunctions.net/api/"
    fun <T> createService(clazz: Class<T>): T {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(clazz)
    }

    enum class State {
        LOADING,
        FAILED,
        SUCCESS
    }
}

data class StatusCode(
    val statusCode: Int
)