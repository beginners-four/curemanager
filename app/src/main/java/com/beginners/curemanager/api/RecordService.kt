package com.beginners.curemanager.api

import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.utils.AppSettings
import retrofit2.Response
import retrofit2.http.*

interface RecordService {
    @GET("exercise/days")
    suspend fun getTotalExerciseDays(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String = ""
    ): Response<TotalExerciseDaysResponse>

    @GET("exercise")
    suspend fun getExerciseRecord(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String = "",
        @Query("start") start:String= AppSettings.FIRST_RELEASE_DAY,
        @Query("end") end :String
    ): Response<ExerciseRecordResponse>

    @GET("exercise/conditions")
    suspend fun getConditionsRecord(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String = "",
        @Query("start") start:String= AppSettings.FIRST_RELEASE_DAY,
        @Query("end") end :String
    ): Response<ConditionRecordResponse>

    @POST("exercise/save")
    suspend fun saveExerciseRecord(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String = "",
        @Body data: ExerciseRecordData
    ): Response<StatusCode>
}

data class ExerciseRecordResponse(
    val statusCode: Int,
    val data: List<ExerciseRecordData>
)

data class TotalExerciseDaysResponse(
    val statusCode: Int,
    val data: TotalDay
)

data class TotalDay(
    val day: Int
)

data class ConditionRecordResponse(
    val statusCode: Int,
    val data: List<ConditionRecord>
)

data class ConditionRecord(
    val date: String,
    val condition: Float
)