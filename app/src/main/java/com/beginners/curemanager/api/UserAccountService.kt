package com.beginners.curemanager.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface UserAccountService {
    @GET("user/create")
    suspend fun createUser(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String
    ): Response<StatusCode>

    @GET("user/handover")
    suspend fun handoverUser(
        @Header("x-uuid") uuid: String,
        @Header("x-uid") uid: String
    ): Response<StatusCode>
}