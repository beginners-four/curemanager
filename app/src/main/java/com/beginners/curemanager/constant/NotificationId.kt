package com.beginners.curemanager.constant

enum class NotificationId(val id: Int) {
    /**
     * エクササイズリマインダー
     */
    EXERCISE_REMINDER(1)
}