package com.beginners.curemanager.core

import android.app.Application
import com.beginners.curemanager.utils.NotificationChannelUtils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CureManagerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        NotificationChannelUtils.initNotificationChannels(this)
    }
}