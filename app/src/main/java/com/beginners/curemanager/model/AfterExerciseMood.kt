package com.beginners.curemanager.model

enum class AfterExerciseMood(val moodPoint: Int) {
    WORST(1),
    NOT_GOOD(2),
    OKAY(3),
    GOOD(4),
    BEST(5)
}