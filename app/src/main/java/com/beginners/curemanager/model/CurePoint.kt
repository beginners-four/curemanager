package com.beginners.curemanager.model

import androidx.annotation.StringRes
import com.beginners.curemanager.R

enum class CurePoint(val id: Int, @StringRes val genreNameResId: Int) {
    ELBOW(R.id.elbow, R.string.cure_parts_elbow),
    KNEES(R.id.knees, R.string.cure_parts_knees),
    BACKACHE(R.id.backache, R.string.cure_parts_backache),
    HEADACHE(R.id.headache, R.string.cure_parts_headache),
    SHOULDER(R.id.stiffness_shoulder, R.string.cure_stiffness_shoulder),
    STIFFNESS_BACK(R.id.stiffness_back, R.string.cure_stiffness_back),
    ASTHENOPIA(R.id.asthenopia, R.string.cure_asthenopia),
    FEEBLENESS(R.id.feebleness, R.string.cure_feebleness),
    ADD_EXERCISE(-1, -1)
}