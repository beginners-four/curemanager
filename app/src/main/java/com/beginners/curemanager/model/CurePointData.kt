package com.beginners.curemanager.model

import java.util.*

data class CurePointData(val curePointData: Pair<CurePoint, Calendar>)