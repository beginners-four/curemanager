package com.beginners.curemanager.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.beginners.curemanager.R

enum class ExerciseMainType(
    val color: Int,
    @DrawableRes val iconResId: Int,
    @StringRes val nameResId: Int
) {
    ACUPOINT(
        R.color.home_color_tsubo,
        R.drawable.ic_tsubo,
        R.string.exercise_genre_pressure_point_massage
    ),
    STRETCH(
        R.color.home_color_stretch,
        R.drawable.ic_stretch,
        R.string.exercise_genre_stretch
    ),
    AEROBIC(
        R.color.home_color_aerobics,
        R.drawable.ic_shoes,
        R.string.exercise_genre_aerobic_exercise
    ),
    STRENGTH(
        R.color.home_color_work_out,
        R.drawable.ic_dumbbell,
        R.string.exercise_genre_work_out
    )
}