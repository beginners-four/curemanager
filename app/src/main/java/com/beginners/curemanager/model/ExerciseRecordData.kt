package com.beginners.curemanager.model

data class ExerciseRecordData(
    val mainType: String?,
    val subType: String?,
    val condition: Int,
    val startTime: String?,
    val endTime: String?,
    val sets: Int?,
    val reps: Int?
)