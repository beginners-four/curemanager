package com.beginners.curemanager.model

enum class ExerciseSubType{
    WALKING,
    RUNNING
}