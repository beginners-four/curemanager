package com.beginners.curemanager.notifications

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.beginners.curemanager.R
import com.beginners.curemanager.constant.NotificationId
import com.beginners.curemanager.utils.AppSettings
import com.beginners.curemanager.utils.NotificationChannelUtils
import com.beginners.curemanager.view.home.HomeActivity

class ExerciseReminderManager(private val context: Context) {

    /**
     * エクササイズリマインダー通知を出す
     */
    @SuppressLint("InlinedApi")
    fun notifyExerciseRemind() {
        if (!AppSettings.getExerciseReminderSetting(context)) return
        val intent = Intent(context, HomeActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent = PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        val title = context.getString(R.string.exercise_reminder_title)
        val message = context.getString(R.string.exercise_reminder_message)

        val notification = NotificationCompat.Builder(
            context,
            NotificationChannelUtils.Channels.EXERCISE_REMINDER.id
        )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(context, R.color.color_primary))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
        NotificationChannelUtils.notify(NotificationId.EXERCISE_REMINDER.id, notification, context)
    }
}