package com.beginners.curemanager.notifications

import android.content.Context
import android.os.Build
import androidx.work.*
import com.beginners.curemanager.BuildConfig
import com.beginners.curemanager.utils.AppSettings
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class ExerciseReminderWorker(val context: Context, params: WorkerParameters) :
    Worker(context, params) {
    override fun doWork(): Result {
        WorkManager.getInstance(applicationContext).enqueue(createOneTimeRequest(context))
        ExerciseReminderManager(context).notifyExerciseRemind()
        return Result.success()
    }

    companion object {
        const val TAG_EXERCISE_REMINDER_WORKER = "tag_exercise_reminder_worker"

        fun createOneTimeRequest(context: Context): WorkRequest {
            val hour = AppSettings.getExerciseRemindHour(context)
            val minute = AppSettings.getExerciseRemindMinute(context)
            val currentDate = Calendar.getInstance()
            val dueDate = Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, hour)
                set(Calendar.MINUTE, minute)
                set(Calendar.SECOND, 0)
            }
            if (dueDate.before(currentDate)) {
                dueDate.add(Calendar.DAY_OF_MONTH, 1)
            }
            val timeDiff = dueDate.timeInMillis.minus(currentDate.timeInMillis)
            return OneTimeWorkRequestBuilder<ExerciseReminderWorker>()
                .setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
                .addTag(TAG_EXERCISE_REMINDER_WORKER)
                .build()
        }
    }
}