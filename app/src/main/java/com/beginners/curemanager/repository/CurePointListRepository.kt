package com.beginners.curemanager.repository

import android.content.Context
import com.beginners.curemanager.utils.AppSettings
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class CurePointListRepository @Inject constructor(@ApplicationContext private val context: Context) {
    var curePointList
        get() = AppSettings.getCurePointList(context)
        set(value) = AppSettings.putCurePointList(context, value)
}