package com.beginners.curemanager.repository

import android.content.Context
import com.beginners.curemanager.api.*
import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.utils.AppSettings
import com.beginners.curemanager.utils.DateFormat
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Response
import java.util.*
import javax.inject.Inject

class RecordRepository @Inject constructor(@ApplicationContext private val context: Context) {
    private val uuid = AppSettings.getUuid(context)
    private val uid = Firebase.auth.currentUser?.uid ?: ""
    private val recordService: RecordService =
        CommonRetrofitService.createService(RecordService::class.java)

    suspend fun getTotalExerciseDays(): Response<TotalExerciseDaysResponse> {
        return recordService.getTotalExerciseDays(uuid, uid)
    }

    suspend fun getExerciseRecord(): Response<ExerciseRecordResponse> {
        return recordService.getExerciseRecord(
            uuid = uuid,
            uid = uid,
            end = DateFormat.yyyyMMdd_hyphen.calToString(Calendar.getInstance())
        )
    }

    suspend fun getConditionsRecord(): Response<ConditionRecordResponse> {
        return recordService.getConditionsRecord(
            uuid = uuid,
            uid = uid,
            end = DateFormat.yyyyMMdd_hyphen.calToString(Calendar.getInstance())
        )
    }

    suspend fun saveExerciseRecord(data: ExerciseRecordData): Response<StatusCode> {
        return recordService.saveExerciseRecord(uuid, uid, data)
    }
}