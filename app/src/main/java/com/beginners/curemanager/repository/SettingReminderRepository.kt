package com.beginners.curemanager.repository

import android.content.Context
import com.beginners.curemanager.utils.AppSettings
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SettingReminderRepository @Inject constructor(@ApplicationContext private val context: Context) {
    var isSettingOn: Boolean
        get() = AppSettings.getExerciseReminderSetting(context)
        set(value) = AppSettings.putExerciseReminderSetting(context, value)

    val remindHour = AppSettings.getExerciseRemindHour(context)
    val remindMinute = AppSettings.getExerciseRemindMinute(context)
    fun setRemindTIme(hour: Int, minute: Int) =
        AppSettings.putExerciseRemindTime(context, hour, minute)
}