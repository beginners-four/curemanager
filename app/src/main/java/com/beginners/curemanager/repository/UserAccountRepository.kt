package com.beginners.curemanager.repository

import com.beginners.curemanager.api.CommonRetrofitService
import com.beginners.curemanager.api.StatusCode
import com.beginners.curemanager.api.UserAccountService
import retrofit2.Response
import javax.inject.Inject

class UserAccountRepository @Inject constructor() {
    private val recordService: UserAccountService =
        CommonRetrofitService.createService(UserAccountService::class.java)

    suspend fun createUser(uuid: String, uid: String): Response<StatusCode> {
        return recordService.createUser(uuid, uid)
    }

    suspend fun handoverUser(uuid: String, uid: String): Response<StatusCode> {
        return recordService.handoverUser(uuid, uid)
    }
}