package com.beginners.curemanager.usecase

import com.beginners.curemanager.repository.CurePointListRepository
import javax.inject.Inject

class CurePointListUseCase @Inject constructor(private val repository: CurePointListRepository) {
    var curePointList
        get() = repository.curePointList
        set(value) {
            repository.curePointList = value
        }
}