package com.beginners.curemanager.usecase

import com.beginners.curemanager.api.ConditionRecordResponse
import com.beginners.curemanager.api.ExerciseRecordResponse
import com.beginners.curemanager.api.StatusCode
import com.beginners.curemanager.api.TotalExerciseDaysResponse
import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.repository.RecordRepository
import retrofit2.Response
import javax.inject.Inject

class RecordUseCase @Inject constructor(private val repository: RecordRepository) {
    suspend fun getTotalExerciseDays(): Response<TotalExerciseDaysResponse> {
        return repository.getTotalExerciseDays()
    }

    suspend fun getExerciseRecord(): Response<ExerciseRecordResponse> {
        return repository.getExerciseRecord()
    }

    suspend fun getConditionsRecord(): Response<ConditionRecordResponse> {
        return repository.getConditionsRecord()
    }

    suspend fun saveExerciseRecord(data: ExerciseRecordData): Response<StatusCode> {
        return repository.saveExerciseRecord(data)
    }
}