package com.beginners.curemanager.usecase

import com.beginners.curemanager.repository.SettingReminderRepository
import com.beginners.curemanager.utils.AppSettings
import javax.inject.Inject

class SettingReminderUseCase @Inject constructor(private val repository: SettingReminderRepository) {
    var isSettingOn: Boolean
        get() = repository.isSettingOn
        set(value) {
            repository.isSettingOn = value
        }

    val remindHour = repository.remindHour
    val remindMinute = repository.remindMinute
    fun setRemindTIme(hour: Int, minute: Int) = repository.setRemindTIme(hour, minute)
}