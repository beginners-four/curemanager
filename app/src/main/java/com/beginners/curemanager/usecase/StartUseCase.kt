package com.beginners.curemanager.usecase

import com.beginners.curemanager.api.StatusCode
import com.beginners.curemanager.repository.UserAccountRepository
import retrofit2.Response
import javax.inject.Inject

class StartUseCase @Inject constructor(private val repository: UserAccountRepository) {
    suspend fun createUser(uuid: String, uid: String): Response<StatusCode> {
        return repository.createUser(uuid, uid)
    }
}