package com.beginners.curemanager.utils

import android.content.Context
import com.beginners.curemanager.model.CurePointData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.time.LocalDateTime
import java.util.*

object AppSettings {
    const val FIRST_RELEASE_DAY = "2023-03-08"
    private const val PREF_APP_SETTING = "pref_app_setting"

    private const val PREF_KEY_APP_FIRST_BOOT_DATE = "pref_key_app_first_boot_date"
    private const val PREF_KEY_APP_LAST_BOOT_DATE = "pref_key_app_last_boot_date"
    private const val PREF_KEY_APP_LAUNCH_COUNT = "pref_key_app_launch_count"
    private const val PREF_KEY_APP_UUID = "pref_key_app_uuid"
    private const val PREF_KEY_EXERCISE_REMINDER_SETTING = "pref_key_exercise_reminder_setting"
    private const val PREF_KEY_EXERCISE_REMINDER_HOUR = "pref_key_exercise_reminder_hour"
    private const val PREF_KEY_EXERCISE_REMINDER_MINUTE = "pref_key_exercise_reminder_minute"
    private const val PREF_KEY_CURE_POINT_LIST = "pref_key_cure_point_list"

    const val ADMOB_AD_UNIT_ID = "ca-app-pub-7318022280747426/4893458163"
    const val ADMOB_AD_UNIT_ID_TEST = "ca-app-pub-3940256099942544/8691691433"

    /**
     * アプリ初回起動日(yyyyMMddHHmm)を取得
     * 厳密にはチュートリアル完了日
     */
    fun getFirstBootDate(context: Context): LocalDateTime? {
        return DateFormat.yyyyMMddHHmm.stringToLocaleDateTime(
            SharedPreferenceUtils.getString(
                context,
                PREF_APP_SETTING,
                PREF_KEY_APP_FIRST_BOOT_DATE,
                ""
            )
        )
    }

    /**
     * アプリ初回起動日(yyyyMMddHHmm)を保存
     * 厳密にはチュートリアル完了日
     */
    fun putFirstBootDate(context: Context) {
        return SharedPreferenceUtils.putString(
            context,
            PREF_APP_SETTING,
            PREF_KEY_APP_FIRST_BOOT_DATE,
            LocalDateTime.now().toString()
        )
    }

    /**
     * アプリ最終起動日(yyyyMMddHHmm)を取得
     */
    fun getLastBootDate(context: Context): LocalDateTime? {
        return DateFormat.yyyyMMddHHmm.stringToLocaleDateTime(
            SharedPreferenceUtils.getString(
                context,
                PREF_APP_SETTING,
                PREF_KEY_APP_LAST_BOOT_DATE,
                ""
            )
        )
    }

    /**
     * アプリ最終起動日(yyyyMMddHHmm)を保存
     */
    fun putLastBootDate(context: Context) {
        return SharedPreferenceUtils.putString(
            context,
            PREF_APP_SETTING,
            PREF_KEY_APP_LAST_BOOT_DATE,
            LocalDateTime.now().toString()
        )
    }

    /**
     * アプリ起動回数を取得
     * チュートリアル未完の場合はカウントしません
     */
    fun getAppLaunchCount(context: Context): Int {
        return SharedPreferenceUtils.getInt(context, PREF_APP_SETTING, PREF_KEY_APP_LAUNCH_COUNT, 0)
    }

    /**
     * アプリ起動回数をカウント
     */
    fun countAppLaunch(context: Context) {
        SharedPreferenceUtils.putInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_APP_LAUNCH_COUNT,
            getAppLaunchCount(context) + 1
        )
    }

    /**
     * チュートリアルを完了したかどうか
     */
    fun isCompleteTutorial(context: Context): Boolean {
        return SharedPreferenceUtils.getInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_APP_LAUNCH_COUNT,
            0
        ) > 0
    }

    fun createUuidIfNeed(context: Context) {
        if (getUuid(context) == "") {
            val uuid = UUID.randomUUID().toString()
            SharedPreferenceUtils.putString(
                context,
                PREF_APP_SETTING,
                PREF_KEY_APP_UUID,
                uuid
            )
        }
    }

    fun getUuid(context: Context): String {
        return SharedPreferenceUtils.getString(
            context,
            PREF_APP_SETTING,
            PREF_KEY_APP_UUID,
            ""
        )
    }

    /**
     * エクササイズリマインダーON/OFFを取得
     */
    fun getExerciseReminderSetting(context: Context): Boolean {
        return SharedPreferenceUtils.getBoolean(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_SETTING,
            true
        )
    }

    /**
     * エクササイズリマインダーON/OFFを保存
     */
    fun putExerciseReminderSetting(context: Context, isOn: Boolean) {
        SharedPreferenceUtils.putBoolean(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_SETTING,
            isOn
        )
    }

    /**
     * エクササイズリマインド時刻（時）を取得
     */
    fun getExerciseRemindHour(context: Context): Int {
        return SharedPreferenceUtils.getInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_HOUR,
            8
        )
    }

    /**
     * エクササイズリマインド時刻（分）を取得
     */
    fun getExerciseRemindMinute(context: Context): Int {
        return SharedPreferenceUtils.getInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_MINUTE,
            0
        )
    }

    /**
     * エクササイズリマインド時刻を保存
     */
    fun putExerciseRemindTime(context: Context, hour: Int, minute: Int) {
        SharedPreferenceUtils.putInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_HOUR,
            hour
        )
        SharedPreferenceUtils.putInt(
            context,
            PREF_APP_SETTING,
            PREF_KEY_EXERCISE_REMINDER_MINUTE,
            minute
        )
    }

    /**
     * お悩みポイントリストを取得
     */
    fun getCurePointList(context: Context): List<CurePointData> {
        val listJson = SharedPreferenceUtils.getString(
            context,
            PREF_APP_SETTING,
            PREF_KEY_CURE_POINT_LIST,
            ""
        )
        return if (listJson.isEmpty()) {
            listOf()
        } else {
            Gson().fromJson(listJson, object : TypeToken<List<CurePointData>>() {}.type)
        }
    }

    /**
     * お悩みポイントリストを保存
     */
    fun putCurePointList(context: Context, list: List<CurePointData>) {
        SharedPreferenceUtils.putString(
            context,
            PREF_APP_SETTING,
            PREF_KEY_CURE_POINT_LIST,
            Gson().toJson(list)
        )
    }
}