package com.beginners.curemanager.utils

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

enum class DateFormat(private val formatString: String) {
    mmss("mm:ss"),
    HHmm("HH:mm"),
    yyyyMMdd("yyyyMMdd"),
    yyyyMMdd_hyphen("yyyy-MM-dd"),
    yyyyMMdd_slash("yyyy/MM/dd"),
    yyyyMMddHHmm("yyyyMMddHHmm"),
    ISO_8601("yyyy-MM-dd'T'HH:mm:ssXXX");

    private fun createFormat(): SimpleDateFormat {
        return SimpleDateFormat(formatString, Locale.JAPAN)
    }

    fun calToString(cal: Calendar): String {
        return createFormat().format(cal.time)
    }

    fun stringToDate(dateString: String): Date? {
        return createFormat().parse(dateString)
    }

    fun stringToCal(dateString: String): Calendar? {
        val cal = Calendar.getInstance()
        try {
            cal.time = createFormat().parse(dateString) ?: return null
        } catch (e: Exception) {
            return null
        }
        return cal
    }

    fun stringToLocaleDate(dateString: String): LocalDate? {
        val date = stringToDate(dateString)
        return LocalDateTime.ofInstant(date?.toInstant(), ZoneId.systemDefault()).toLocalDate()
    }

    fun stringToLocaleDateTime(dateString: String): LocalDateTime? {
        return  LocalDateTime.parse(dateString)
    }

    fun longToString(time: Long): String {
        return createFormat().format(time)
    }


    companion object {
        fun isSameDate(cal1: Calendar, cal2: Calendar): Boolean {
            if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) return false
            if (cal1.get(Calendar.DAY_OF_YEAR) != cal2.get(Calendar.DAY_OF_YEAR)) return false
            return true
        }
    }
}