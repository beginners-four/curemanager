package com.beginners.curemanager.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.beginners.curemanager.R

object NotificationChannelUtils {
    enum class Channels(val id: String) : NotificationChannelCreator {

        EXERCISE_REMINDER("exercise_reminder") {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun create(context: Context): NotificationChannel {
                val channel = NotificationChannel(
                    id,
                    context.getString(R.string.notification_channel_exercise_reminder),
                    NotificationManager.IMPORTANCE_HIGH
                )
                return channel.apply {
                    enableVibration(true)
                    enableLights(true)
                }
            }
        },
    }

    internal interface NotificationChannelCreator {
        fun create(context: Context): NotificationChannel?
    }

    /**
     * チャンネルの初期化
     */
    fun initNotificationChannels(context: Context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager ?: return
        for (channel in Channels.values()) {
            channel.create(context)?.let {
                notificationManager.createNotificationChannel(it)
            }
        }
    }

    /**
     * 指定したIDのNotificationの通知を出す
     */
    fun notify(notificationId: Int, notification: Notification, context: Context) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager ?: return
        notificationManager.notify(notificationId, notification)
    }
}