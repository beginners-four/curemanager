package com.beginners.curemanager.utils

import android.content.Context

object SharedPreferenceUtils {

    /**
     * プリファレンスにキーが含まれているかどうか
     */
    fun contains(context: Context, preferenceName: String, key: String): Boolean {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).contains(key)
    }

    /**
     * プリファレンスからBoolean型の値を取得
     */
    fun getBoolean(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Boolean
    ): Boolean {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getBoolean(key, defaultValue)
    }

    /**
     * プリファレンスからInt型の値を取得
     */
    fun getInt(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Int
    ): Int {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getInt(key, defaultValue)
    }

    /**
     * プリファレンスからFloat型の値を取得
     */
    fun getFloat(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Float
    ): Float {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getFloat(key, defaultValue)
    }

    /**
     * プリファレンスからLong型の値を取得
     */
    fun getFloat(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Long
    ): Long {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getLong(key, defaultValue)
    }

    /**
     * プリファレンスからString型の値を取得
     */
    fun getString(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: String
    ): String {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getString(key, defaultValue) ?: defaultValue
    }

    /**
     * プリファレンスからSet<String>型の値を取得
     */
    fun getStringSet(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Set<String>?
    ): Set<String>? {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
            .getStringSet(key, defaultValue)
    }

    /**
     * プリファレンスにBoolean型の値を保存
     */
    fun putBoolean(context: Context, preferenceName: String, key: String, value: Boolean) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putBoolean(key, value).apply()
    }

    /**
     * プリファレンスにInt型の値を保存
     */
    fun putInt(context: Context, preferenceName: String, key: String, value: Int) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putInt(key, value).apply()
    }

    /**
     * プリファレンスにFloat型の値を保存
     */
    fun putFloat(context: Context, preferenceName: String, key: String, value: Float) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putFloat(key, value).apply()
    }

    /**
     * プリファレンスにLong型の値を保存
     */
    fun putLong(context: Context, preferenceName: String, key: String, value: Long) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putLong(key, value).apply()
    }

    /**
     * プリファレンスにString型の値を保存
     */
    fun putString(context: Context, preferenceName: String, key: String, value: String) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putString(key, value).apply()
    }

    /**
     * プリファレンスにSet<String>型の値を保存
     */
    fun putStringSet(context: Context, preferenceName: String, key: String, value: Set<String>) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit()
            .putStringSet(key, value).apply()
    }

    /**
     * 指定したプリファレンスに保存されている値をすべてクリア
     */
    fun clear(context: Context, preferenceName: String) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().clear().apply()
    }

    /**
     * 指定したプリファレンスの指定したkeyに対応する値を削除します
     */
    fun remove(context: Context, preferenceName: String, key: String) {
        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().remove(key)
            .apply()
    }
}