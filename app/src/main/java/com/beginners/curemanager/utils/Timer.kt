package com.beginners.curemanager.utils

import kotlinx.coroutines.delay

class Timer(private val targetFragment: TimerListener, private val intervalTime: Int) {
    interface TimerListener {
        fun onTick(lateSec: Long)
        fun onFinish()
    }

    suspend fun countDownStart() {
        var lateSec = intervalTime  //計算用に格納
        var delayTime = 1000L   //呼び出す間隔 1秒おき
        val startTime = System.currentTimeMillis() //スタート時の時刻を保持
        var count = 0 //while内の繰り返し数カウント
        var diffTime: Long

        while (lateSec > 0) {
            //ここで指定した時間待つので、以降の処理はここで指定された間隔で実行する
            delay(delayTime)

            diffTime = System.currentTimeMillis() - startTime - count * 1000   //現在時刻から開始時刻と経過秒数を引く
            delayTime = 1000 - diffTime         //次のdelay待ち時間を補正
            lateSec -= 1                        //1秒ずつマイナス
            count += 1                          //繰り返し回数をカウント

            targetFragment.onTick((lateSec * 1000).toLong())

            if (lateSec == 0) {
                targetFragment.onFinish()
            }
        }
    }
}