package com.beginners.curemanager.view

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.NumberPicker
import androidx.fragment.app.DialogFragment
import com.beginners.curemanager.R

//TODO DataBindingで書き換える
class NumberPickerDialog : DialogFragment(){
    var listener: NumberPickerListener? = null

    private val title: String
        get() = checkNotNull(arguments?.getString(BUNDLE_KEY_TITLE))

    private val initialNum: Int
        get() = checkNotNull(arguments?.getInt(BUNDLE_KEY_INITIAL_NUM))

    interface NumberPickerListener {
        fun onNumberPick(pickedNum: Int)
    }

    /**
     * ダイアログ作成
     **/
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater = requireActivity().layoutInflater
        val dialogView = inflater.inflate(R.layout.number_picker_dialog, null)
        val builder = AlertDialog.Builder(context)

        // Dialogの設定
        builder.setView(dialogView)
        builder.setTitle(title)
        builder.setPositiveButton(requireContext().getText(R.string.ok)) { _, _ ->
            listener?.onNumberPick(dialogView.findViewById<NumberPicker>(R.id.number_picker).value)
        }

        // NumberPickerの設定
        val np = dialogView.findViewById<NumberPicker>(R.id.number_picker)
        np.minValue = 1
        np.maxValue = 60
        np.value = initialNum

        return builder.create()
    }

    companion object {
        private const val BUNDLE_KEY_TITLE = "bundle_key_title"
        private const val BUNDLE_KEY_INITIAL_NUM = "bundle_key_initial_num"

        fun newInstance(title: String, initialNum: Int) = NumberPickerDialog().apply {
            val bundle = Bundle()
            bundle.putString(BUNDLE_KEY_TITLE, title)
            bundle.putInt(BUNDLE_KEY_INITIAL_NUM, initialNum)
            arguments = bundle
        }
    }
}