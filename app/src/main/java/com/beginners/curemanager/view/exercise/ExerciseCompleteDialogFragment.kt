package com.beginners.curemanager.view.exercise

import android.app.Dialog
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.beginners.curemanager.R
import com.beginners.curemanager.databinding.ExerciseCompleteDialogBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class ExerciseCompleteDialogFragment : DialogFragment() {
    private var _binding: ExerciseCompleteDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = DataBindingUtil.inflate(
            requireActivity().layoutInflater,
            R.layout.exercise_complete_dialog,
            null,
            false
        )
        binding.dialogClose.setOnClickListener {
            dismiss()
        }

        val builder = MaterialAlertDialogBuilder(requireContext())
        return builder.setView(_binding?.root).create()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance() = ExerciseCompleteDialogFragment()
    }
}