package com.beginners.curemanager.view.exercise

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.beginners.curemanager.R
import com.beginners.curemanager.databinding.FragmentRecordMoodBinding
import com.beginners.curemanager.model.AfterExerciseMood
import com.beginners.curemanager.view.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecordMoodFragment : Fragment(), RecordMoodNavigator {

    private var _binding: FragmentRecordMoodBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModels<RecordMoodViewModel>()
    private val args: RecordMoodFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecordMoodBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@RecordMoodFragment
            viewModel = this@RecordMoodFragment.viewModel.apply {
                navigator = this@RecordMoodFragment
            }
            recordMoodCompleteButton.setOnClickListener {
                this@RecordMoodFragment.viewModel.saveRecord(
                    args.mainType,
                    args.subType,
                    args.startTime,
                    args.endTime,
                    if (args.sets == 0) null else args.sets,
                    if (args.reps == 0) null else args.reps
                )
            }
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun completeRecord() {
        requireActivity().finish()
        startActivity(HomeActivity.createIntent(requireContext(), true))
    }

    override fun selectMood() {
        when (viewModel.selectedMood.value) {
            AfterExerciseMood.WORST -> {
                binding.moodIconWorst.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_1_on,
                        null
                    )
                )
                binding.moodIconNotGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_2_off,
                        null
                    )
                )
                binding.moodIconOkay.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_3_off,
                        null
                    )
                )
                binding.moodIconGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_4_off,
                        null
                    )
                )
                binding.moodIconBest.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_5_off,
                        null
                    )
                )
            }
            AfterExerciseMood.NOT_GOOD -> {
                binding.moodIconWorst.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_1_off,
                        null
                    )
                )
                binding.moodIconNotGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_2_on,
                        null
                    )
                )
                binding.moodIconOkay.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_3_off,
                        null
                    )
                )
                binding.moodIconGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_4_off,
                        null
                    )
                )
                binding.moodIconBest.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_5_off,
                        null
                    )
                )
            }
            AfterExerciseMood.OKAY -> {
                binding.moodIconWorst.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_1_off,
                        null
                    )
                )
                binding.moodIconNotGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_2_off,
                        null
                    )
                )
                binding.moodIconOkay.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_3_on,
                        null
                    )
                )
                binding.moodIconGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_4_off,
                        null
                    )
                )
                binding.moodIconBest.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_5_off,
                        null
                    )
                )
            }
            AfterExerciseMood.GOOD -> {
                binding.moodIconWorst.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_1_off,
                        null
                    )
                )
                binding.moodIconNotGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_2_off,
                        null
                    )
                )
                binding.moodIconOkay.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_3_off,
                        null
                    )
                )
                binding.moodIconGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_4_on,
                        null
                    )
                )
                binding.moodIconBest.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_5_off,
                        null
                    )
                )
            }
            AfterExerciseMood.BEST -> {
                binding.moodIconWorst.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_1_off,
                        null
                    )
                )
                binding.moodIconNotGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_2_off,
                        null
                    )
                )
                binding.moodIconOkay.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_3_off,
                        null
                    )
                )
                binding.moodIconGood.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_4_off,
                        null
                    )
                )
                binding.moodIconBest.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.face_5_on,
                        null
                    )
                )
            }
            else -> {
                //do nothing
            }
        }
    }

    override fun showErrorToast() {
        Toast.makeText(
            requireContext(),
            getString(R.string.network_fail),
            Toast.LENGTH_SHORT
        ).show()
    }
}