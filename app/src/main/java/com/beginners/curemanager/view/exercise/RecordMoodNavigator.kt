package com.beginners.curemanager.view.exercise

interface RecordMoodNavigator {
    fun completeRecord()
    fun selectMood()
    fun showErrorToast()
}