package com.beginners.curemanager.view.exercise

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beginners.curemanager.model.AfterExerciseMood
import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.usecase.RecordUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecordMoodViewModel @Inject constructor(val useCase: RecordUseCase) : ViewModel() {
    var navigator: RecordMoodNavigator? = null

    private val exceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, _ ->
            navigator?.showErrorToast()
        }

    private val _selectedMood = MutableLiveData(AfterExerciseMood.GOOD)
    val selectedMood: LiveData<AfterExerciseMood>
        get() = _selectedMood

    fun onClickMoodIcon(mood: AfterExerciseMood) {
        _selectedMood.value = mood
        navigator?.selectMood()
    }

    fun saveRecord(
        mainType: String,
        subType: String,
        startTime: String,
        sendTime: String,
        sets: Int?,
        reps: Int?
    ) {
        viewModelScope.launch(exceptionHandler) {
            val moodPoint = _selectedMood.value?.moodPoint ?: 0
            useCase.saveExerciseRecord(
                ExerciseRecordData(
                    mainType,
                    subType,
                    moodPoint,
                    startTime,
                    sendTime,
                    sets,
                    reps
                )
            )
            navigator?.completeRecord()
        }
    }
}