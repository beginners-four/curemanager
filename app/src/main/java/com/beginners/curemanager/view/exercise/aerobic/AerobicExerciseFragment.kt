package com.beginners.curemanager.view.exercise.aerobic

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.beginners.curemanager.BuildConfig
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsParamBuilder
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentAerobicExerciseBinding
import com.beginners.curemanager.model.ExerciseMainType
import com.beginners.curemanager.utils.AppSettings
import com.beginners.curemanager.utils.DateFormat
import com.beginners.curemanager.utils.Timer
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.util.*

class AerobicExerciseFragment : Fragment(), AerobicExerciseNavigator, Timer.TimerListener {

    private var _binding: FragmentAerobicExerciseBinding? = null
    private val binding get() = _binding!!
    private val args: AerobicExerciseFragmentArgs by navArgs()
    private val viewModel by viewModels<AerobicExerciseViewModel>()
    private val scope = MainScope()
    private lateinit var callback: OnBackPressedCallback
    private var mInterstitialAd: InterstitialAd? = null
    private val adUnitId = if (BuildConfig.DEBUG) {
        AppSettings.ADMOB_AD_UNIT_ID_TEST
    } else {
        AppSettings.ADMOB_AD_UNIT_ID
    }

    private var endTime = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = requireActivity().onBackPressedDispatcher.addCallback {
            showSuspendDialog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            requireContext(),
            adUnitId,
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {}

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd.also {
                        it.fullScreenContentCallback = object : FullScreenContentCallback() {
                            override fun onAdDismissedFullScreenContent() {
                                showNextPage()
                            }

                            override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                                showNextPage()
                            }

                            override fun onAdShowedFullScreenContent() {}
                        }
                    }
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAerobicExerciseBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@AerobicExerciseFragment
            viewModel = this@AerobicExerciseFragment.viewModel.apply {
                navigator = this@AerobicExerciseFragment
            }
        }
        scope.launch {
            //Viewへの描画が追いついてないせいか1分でセットしても58秒からのカウントダウンしか表示されないので＋2している
            Timer(this@AerobicExerciseFragment, args.exerciseTime + 2).countDownStart()
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        scope.cancel()
        callback.remove()
    }

    override fun showEarlyFinishDialog() {
        val title = requireContext().getString(R.string.exercise_early_finish_dialog_title)
        val message = requireContext().getString(R.string.exercise_early_finish_dialog_message)
        val positiveButtonText = requireContext().getString(R.string.yes)
        val dialog = MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(positiveButtonText) { _, _ ->
                showAd()

                FirebaseAnalyticsUtils.sendEvent(
                    requireContext(),
                    FirebaseEvent.Name.EXERCISE,
                    FirebaseAnalyticsParamBuilder().putParam(
                        FirebaseEvent.Key.ACTION,
                        FirebaseEvent.Value.early_finish).build()
                )
            }
        }.create()

        dialog.show()
    }

    override fun onTick(lateSec: Long) {
        //画面に残り時間を表示
        binding.timer.text = DateFormat.mmss.longToString(lateSec)
    }

    override fun onFinish() {
        vibrate()
        showAd()

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.EXERCISE,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.ACTION,
                FirebaseEvent.Value.complete).build()
        )
    }

    private fun vibrate() {
        val vibrator = requireContext().getSystemService(VIBRATOR_SERVICE) as Vibrator
        val pattern = longArrayOf(0, 500, 500, 500, 500, 1000)
        vibrator.vibrate(VibrationEffect.createWaveform(pattern, -1))
    }

    private fun showSuspendDialog() {
        val title = requireContext().getString(R.string.exercise_suspend_dialog_title)
        val message = requireContext().getString(R.string.exercise_suspend_dialog_message)
        val positiveButtonText = requireContext().getString(R.string.yes)
        val negativeButtonText = requireContext().getString(R.string.no)
        val dialog = MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(positiveButtonText) { _, _ ->
                findNavController().popBackStack()

                FirebaseAnalyticsUtils.sendEvent(
                    requireContext(),
                    FirebaseEvent.Name.EXERCISE,
                    FirebaseAnalyticsParamBuilder().putParam(
                        FirebaseEvent.Key.ACTION,
                        FirebaseEvent.Value.suspend).build()
                )
            }
            setNegativeButton(negativeButtonText) { _, _ -> }
        }.create()

        dialog.show()
    }

    private fun showAd() {
        endTime = DateFormat.ISO_8601.calToString(Calendar.getInstance())
        if (mInterstitialAd != null) {
            mInterstitialAd?.show(requireActivity())
        } else {
            showNextPage()
        }
    }

    private fun showNextPage() {
        mInterstitialAd = null

        val navHostFragment =
            requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_home) as NavHostFragment
        val navController = navHostFragment.navController
        //TODO ツボ、ストレッチ、ボディメイクも含めた共通のfragmentにする
        val action =
            AerobicExerciseFragmentDirections.actionAerobicExerciseFragmentToRecordMoodFragment(
                ExerciseMainType.AEROBIC.name,
                args.subType,
                args.startTime,
                endTime,
                0,
                0
            )
        navController.navigate(action)
    }
}