package com.beginners.curemanager.view.exercise.aerobic

interface AerobicExerciseNavigator {
    fun showEarlyFinishDialog()
}