package com.beginners.curemanager.view.exercise.aerobic

import androidx.lifecycle.ViewModel

class AerobicExerciseViewModel : ViewModel() {
    var navigator: AerobicExerciseNavigator? = null

    fun onClickEarlyFinishButton() {
        navigator?.showEarlyFinishDialog()
    }
}