package com.beginners.curemanager.view.exercise.aerobic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsParamBuilder
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentPreAerobicExerciseBinding
import com.beginners.curemanager.model.ExerciseMainType
import com.beginners.curemanager.utils.DateFormat
import com.beginners.curemanager.view.NumberPickerDialog
import java.util.*

class PreAerobicExerciseFragment : Fragment(), PreAerobicExerciseNavigator,
    NumberPickerDialog.NumberPickerListener {

    private var _binding: FragmentPreAerobicExerciseBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<PreAerobicExerciseViewModel>()

    private var selectedCustomTime = 20

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.navigator = this
        _binding = FragmentPreAerobicExerciseBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@PreAerobicExerciseFragment
            viewModel = this@PreAerobicExerciseFragment.viewModel
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showNumberPickerDialog() {
        val dialog =
            NumberPickerDialog.newInstance(
                requireContext().getString(R.string.pre_exercise_select_time_custom_title),
                selectedCustomTime
            )
        dialog.listener = this
        dialog.show(parentFragmentManager, "NumberPickerDialog")
    }

    override fun showNextPage() {
        val exerciseTimeMinutes = when (binding.radioGroup.checkedRadioButtonId) {
            R.id.radio_button_5 -> 5
            R.id.radio_button_15 -> 15
            R.id.radio_button_20 -> 20
            R.id.radio_button_custom -> selectedCustomTime
            else -> 5
        }
        val navHostFragment =
            requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_home) as NavHostFragment
        val navController = navHostFragment.navController
        val subType = viewModel.selectedSubType.value?.name ?: return
        val startTime = DateFormat.ISO_8601.calToString(Calendar.getInstance())
        val action =
            PreAerobicExerciseFragmentDirections.actionPreAerobicExerciseFragmentToAerobicExerciseFragment(
                ExerciseMainType.AEROBIC.name,
                subType,
                exerciseTimeMinutes * 60,
                startTime
            )
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.PRE_EXERCISE,
            FirebaseAnalyticsParamBuilder().putParam(FirebaseEvent.Key.ACTION,FirebaseEvent.Value.start).build()
        )
    }

    override fun onNumberPick(pickedNum: Int) {
        viewModel.setFlexibleTime(pickedNum)
        selectedCustomTime = pickedNum
    }
}