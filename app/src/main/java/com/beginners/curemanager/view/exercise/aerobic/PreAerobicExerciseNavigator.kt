package com.beginners.curemanager.view.exercise.aerobic

interface PreAerobicExerciseNavigator {
    fun showNumberPickerDialog()
    fun showNextPage()
}