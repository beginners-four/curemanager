package com.beginners.curemanager.view.exercise.aerobic

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beginners.curemanager.model.ExerciseSubType

class PreAerobicExerciseViewModel : ViewModel() {
    var navigator: PreAerobicExerciseNavigator? = null

    private val _flexibleTime = MutableLiveData<Int?>()
    val flexibleTime: LiveData<Int?>
        get() = _flexibleTime

    private val _selectedSubType = MutableLiveData(ExerciseSubType.WALKING)
    val selectedSubType: LiveData<ExerciseSubType>
        get() = _selectedSubType

    fun onClickCustomMinutes() {
        navigator?.showNumberPickerDialog()
    }

    fun onClickStart() {
        navigator?.showNextPage()
    }

    fun setFlexibleTime(time: Int) {
        _flexibleTime.value = time
    }

    fun onClickSubTypeRadioButton(type: ExerciseSubType) {
        _selectedSubType.value = type
    }
}