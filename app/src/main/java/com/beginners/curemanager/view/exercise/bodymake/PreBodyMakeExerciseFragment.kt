package com.beginners.curemanager.view.exercise.bodymake

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.beginners.curemanager.databinding.FragmentPreBodyMakeExerciseBinding

class PreBodyMakeExerciseFragment : Fragment() {

    private var _binding: FragmentPreBodyMakeExerciseBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPreBodyMakeExerciseBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@PreBodyMakeExerciseFragment
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}