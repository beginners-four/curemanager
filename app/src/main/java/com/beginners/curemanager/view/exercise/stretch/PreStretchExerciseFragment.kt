package com.beginners.curemanager.view.exercise.stretch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.beginners.curemanager.databinding.FragmentPreStretchExerciseBinding

class PreStretchExerciseFragment : Fragment() {

    private var _binding: FragmentPreStretchExerciseBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPreStretchExerciseBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@PreStretchExerciseFragment
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}