package com.beginners.curemanager.view.exercise.tsubo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.beginners.curemanager.databinding.FragmentPreTsuboExerciseBinding

class PreTsuboExerciseFragment : Fragment() {

    private var _binding: FragmentPreTsuboExerciseBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPreTsuboExerciseBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@PreTsuboExerciseFragment
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}