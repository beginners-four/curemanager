package com.beginners.curemanager.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.beginners.curemanager.databinding.TargetBodyPartsItemBinding
import com.beginners.curemanager.model.CurePointData


class CurePointListAdapter(
    private val viewLifecycleOwner: LifecycleOwner,
    private val viewModel: HomeViewModel
) : ListAdapter<CurePointData, CurePointListAdapter.CurePointViewHolder>(DiffCallback) {

    class CurePointViewHolder(
        private val binding: TargetBodyPartsItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CurePointData, viewLifecycleOwner: LifecycleOwner, viewModel: HomeViewModel) {
            binding.run {
                lifecycleOwner = viewLifecycleOwner
                curePoint = item.curePointData.first
                addDate = item.curePointData.second
                this.viewModel = viewModel
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurePointViewHolder {
        val binding = TargetBodyPartsItemBinding.inflate(LayoutInflater.from(parent.context))
        return CurePointViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurePointViewHolder, position: Int) {
        holder.bind(getItem(position), viewLifecycleOwner, viewModel)
    }
}

private object DiffCallback : DiffUtil.ItemCallback<CurePointData>() {
    override fun areItemsTheSame(oldItem: CurePointData, newItem: CurePointData): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: CurePointData, newItem: CurePointData): Boolean {
        return oldItem == newItem
    }
}