package com.beginners.curemanager.view.home

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FirstBootCompleteDialogBinding

class FirstBootCompleteDialogFragment : DialogFragment() {
    private var _binding: FirstBootCompleteDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        FirebaseAnalyticsUtils.sendEvent(requireContext(), FirebaseEvent.Name.TUTORIAL_COMPLETE)
        _binding = DataBindingUtil.inflate(
            requireActivity().layoutInflater,
            R.layout.first_boot_complete_dialog,
            null,
            false
        )
        binding.dialogOk.setOnClickListener {
            dismiss()
        }

        val builder = AlertDialog.Builder(requireContext())
        return builder.setView(_binding?.root).create()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance() = FirstBootCompleteDialogFragment()
    }
}