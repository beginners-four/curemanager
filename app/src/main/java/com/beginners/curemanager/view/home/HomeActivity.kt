package com.beginners.curemanager.view.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.beginners.curemanager.R
import com.beginners.curemanager.databinding.ActivityHomeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isShowExerciseCompleteDialog =
            intent.getBooleanExtra(IS_SHOW_EXERCISE_COMPLETE_DIALOG, false)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.bottomNav

        val navController =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_home)
                ?.findNavController()?.apply {
                    addOnDestinationChangedListener { _, destination, _ ->
                        binding.bottomNav.visibility =
                                //各タブのTOP画面以外は下タブ非表示
                            if (destination.id == R.id.navigation_home
                                || destination.id == R.id.navigation_record
                                || destination.id == R.id.navigation_settings
                            ) View.VISIBLE else View.GONE
                    }
                    setGraph(
                        R.navigation.mobile_navigation,
                        HomeFragmentArgs(isShowExerciseCompleteDialog).toBundle()
                    )
                }
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
                R.id.navigation_record,
                R.id.navigation_settings,
                R.id.aerobicExerciseFragment
            )
        )
        if (navController != null) {
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)
        }
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.nav_host_fragment_activity_home).navigateUp()


    companion object {
        private const val IS_SHOW_EXERCISE_COMPLETE_DIALOG = "is_show_exercise_complete_dialog"
        fun createIntent(context: Context, isShowExerciseCompleteDialog: Boolean = false): Intent {
            return Intent(context, HomeActivity::class.java).apply {
                putExtra(IS_SHOW_EXERCISE_COMPLETE_DIALOG, isShowExerciseCompleteDialog)
            }
        }
    }
}