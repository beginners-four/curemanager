package com.beginners.curemanager.view.home

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsParamBuilder
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentHomeBinding
import com.beginners.curemanager.model.CurePoint
import com.beginners.curemanager.utils.AppSettings
import com.beginners.curemanager.view.exercise.ExerciseCompleteDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), HomeNavigator {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val args: HomeFragmentArgs by navArgs()
    private lateinit var navController: NavController

    private val viewModel by activityViewModels<HomeViewModel>()
    private lateinit var curePointListAdapter: CurePointListAdapter

    private var isShowedExerciseCompleteDialog = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        AppSettings.countAppLaunch(requireContext())
        navController =
            (requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_home) as NavHostFragment).navController

        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        viewModel.navigator = this
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        if (AppSettings.getAppLaunchCount(requireContext()) == 1) {
            //初回起動時のみようこそダイアログ表示
            val dialog = FirstBootCompleteDialogFragment.newInstance()
            dialog.show(parentFragmentManager, "FirstBootCompleteDialogFragment")

            AppSettings.putFirstBootDate(requireContext())
        }
        AppSettings.putLastBootDate(requireContext())

        if (args.isShowExerciseCompleteDialog && !isShowedExerciseCompleteDialog) {
            val dialog = ExerciseCompleteDialogFragment.newInstance()
            dialog.show(parentFragmentManager, "ExerciseCompleteDialogFragment")
            isShowedExerciseCompleteDialog = true
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        viewModel.run {
            curePointList.observe(viewLifecycleOwner) {
                curePointListAdapter.run {
                    submitList(it)
                    notifyDataSetChanged()
                }
            }

            setIndicatorText(
                requireContext().getString(
                    R.string.home_indicator,
                    1,
                    AppSettings.getCurePointList(requireContext()).size - 1
                )
            )
            setIndicatorTextVisible(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUpRecyclerView() {
        val recyclerView = binding.targetBodyPartsRecycler.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            //追加/変更画面から戻ってきたときにカードがずれた位置で止まってしまう問題の対策
            (layoutManager as LinearLayoutManager).scrollToPosition(0)
            adapter = CurePointListAdapter(viewLifecycleOwner, viewModel).also {
                curePointListAdapter = it
            }
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                val layoutManager = (getLayoutManager() as LinearLayoutManager)
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val lastVisibleItemPosition =
                        layoutManager.findFirstCompletelyVisibleItemPosition()
                    val itemTotalCount = (recyclerView.adapter?.itemCount ?: 0) - 1
                    if (lastVisibleItemPosition == itemTotalCount && !canScrollHorizontally(1)) {
                        viewModel.setIsAddChangeButton(true)
                    } else {
                        viewModel.setIsAddChangeButton(false)
                    }
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    when (newState) {
                        RecyclerView.SCROLL_STATE_IDLE -> {
                            viewModel.setIndicatorText(
                                requireContext().getString(
                                    R.string.home_indicator,
                                    layoutManager.findFirstCompletelyVisibleItemPosition() + 1,
                                    AppSettings.getCurePointList(requireContext()).size - 1
                                )
                            )
                            viewModel.setIndicatorTextVisible(viewModel.isAddChangeButton.value == false)
                        }
                        else -> {
                            viewModel.setIndicatorTextVisible(false)
                        }
                    }
                }
            })
            view?.post {
                addItemDecoration(object : RecyclerView.ItemDecoration() {
                    override fun getItemOffsets(
                        outRect: Rect,
                        view: View,
                        parent: RecyclerView,
                        state: RecyclerView.State
                    ) {
                        val edgeMargin =
                            ((parent.width - context.resources.getDimensionPixelSize(R.dimen.home_genre_card_width)) / 2)
                        val margin = context.resources.getDimensionPixelSize(R.dimen.space_small)
                        val position = parent.getChildAdapterPosition(view)
                        outRect.left = if (position == 0) {
                            edgeMargin
                        } else {
                            margin
                        }
                        outRect.right = if (position == state.itemCount - 1) {
                            edgeMargin
                        } else {
                            margin
                        }
                    }
                })
            }
        }
        PagerSnapHelper().attachToRecyclerView(recyclerView)
    }

    override fun showPreTsuboExerciseFragment() {
        val action = HomeFragmentDirections.actionNavigationHomeToPreTsuboExerciseFragment()
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.HOME_EXERCISE_BUTTON,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TYPE,
                FirebaseEvent.Value.acupoint
            ).build()
        )
    }

    override fun showPreStretchExerciseFragment() {
        val action = HomeFragmentDirections.actionNavigationHomeToPreStretchExerciseFragment()
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.HOME_EXERCISE_BUTTON,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TYPE,
                FirebaseEvent.Value.stretch
            ).build()
        )
    }

    override fun showPreAerobicExerciseFragment() {
        val action = HomeFragmentDirections.actionNavigationHomeToPreAerobicExerciseFragment()
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.HOME_EXERCISE_BUTTON,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TYPE,
                FirebaseEvent.Value.aerobic
            ).build()
        )
    }

    override fun showPreBodyMakeExerciseFragment() {
        val action = HomeFragmentDirections.actionNavigationHomeToPreBodyMakeExerciseFragment()
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.HOME_EXERCISE_BUTTON,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TYPE,
                FirebaseEvent.Value.body_make
            ).build()
        )
    }

    override fun showEditCurePointCardFragment() {
        val action = HomeFragmentDirections.actionNavigationHomeToSelectBodyPartsFragment()
        navController.navigate(action)
    }

    override fun showConfirmDeleteDialog(curePoint: CurePoint) {
        val title = requireContext().getString(R.string.home_cure_card_delete_dialog_title)
        val positiveButtonText = requireContext().getString(R.string.yes)
        val negativeButtonText = requireContext().getString(R.string.no)
        val dialog = MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(title)
            setPositiveButton(positiveButtonText) { _, _ ->
                val curePointList =
                    viewModel.curePointList.value?.map { it.curePointData.first }?.toMutableList()
                curePointList?.remove(curePoint)
                viewModel.setCurePointList(curePointList?.toList() ?: return@setPositiveButton)
            }
            setNegativeButton(negativeButtonText) { _, _ -> }
        }.create()

        dialog.show()
    }
}