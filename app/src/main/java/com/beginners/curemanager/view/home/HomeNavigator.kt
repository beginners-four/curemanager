package com.beginners.curemanager.view.home

import com.beginners.curemanager.model.CurePoint

interface HomeNavigator {
    fun showPreTsuboExerciseFragment()
    fun showPreStretchExerciseFragment()
    fun showPreAerobicExerciseFragment()
    fun showPreBodyMakeExerciseFragment()
    fun showEditCurePointCardFragment()
    fun showConfirmDeleteDialog(curePoint: CurePoint)
}