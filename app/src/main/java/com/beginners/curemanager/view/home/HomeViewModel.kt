package com.beginners.curemanager.view.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beginners.curemanager.model.CurePoint
import com.beginners.curemanager.model.CurePointData
import com.beginners.curemanager.usecase.CurePointListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val useCase: CurePointListUseCase) : ViewModel() {
    var navigator: HomeNavigator? = null

    private val _isAddChangeButton = MutableLiveData(false)
    val isAddChangeButton: LiveData<Boolean>
        get() = _isAddChangeButton

    private val _curePointList = MutableLiveData(useCase.curePointList)
    val curePointList: LiveData<List<CurePointData>>
        get() = _curePointList

    private val _isIndicatorTextVisible = MutableLiveData(true)
    val isIndicatorTextVisible: LiveData<Boolean>
        get() = _isIndicatorTextVisible

    private val _indicatorText = MutableLiveData("")
    val indicatorText: LiveData<String>
        get() = _indicatorText

    fun setIsAddChangeButton(bool: Boolean) {
        _isAddChangeButton.value = bool
    }

    fun setCurePointList(list: List<CurePoint>) {
        val curePointListAddDate = mutableListOf<CurePointData>()
        list.forEach { curePoint ->
            if (useCase.curePointList.map { it.curePointData.first }.contains(curePoint)) {
                val curePointListMap = useCase.curePointList.associate { it.curePointData }
                curePointListAddDate.add(CurePointData(Pair(curePoint, curePointListMap[curePoint] ?: return@forEach)))
            } else {
                curePointListAddDate.add(CurePointData(Pair(curePoint, Calendar.getInstance())))
            }
        }
        _curePointList.value = curePointListAddDate
        useCase.curePointList = curePointListAddDate
    }

    fun setIndicatorTextVisible(bool: Boolean) {
        _isIndicatorTextVisible.value = bool
    }

    fun setIndicatorText(text: String) {
        _indicatorText.value = text
    }

    fun onClickDeleteButton(curePoint: CurePoint) {
        navigator?.showConfirmDeleteDialog(curePoint)
    }

    fun onClickAddChangeButton() {
        navigator?.showEditCurePointCardFragment()
    }

    fun onClickTsuboExercise() {
        navigator?.showPreTsuboExerciseFragment()
    }

    fun onClickStretchExercise() {
        navigator?.showPreStretchExerciseFragment()
    }

    fun onClickAerobicExercise() {
        navigator?.showPreAerobicExerciseFragment()
    }

    fun onClickBodyMakeExercise() {
        navigator?.showPreBodyMakeExerciseFragment()
    }
}