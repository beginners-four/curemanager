package com.beginners.curemanager.view.introduction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentFirstIntroductionBinding

class FirstIntroductionFragment : Fragment() {

    private var _binding: FragmentFirstIntroductionBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        FirebaseAnalyticsUtils.sendEvent(requireContext(), FirebaseEvent.Name.TUTORIAL_BEGIN)

        _binding = FragmentFirstIntroductionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance() = FirstIntroductionFragment()
    }
}