package com.beginners.curemanager.view.introduction

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.beginners.curemanager.R
import com.beginners.curemanager.databinding.ActivityStartBinding
import com.beginners.curemanager.utils.AppSettings
import com.beginners.curemanager.view.home.HomeActivity
import com.beginners.curemanager.view.selectbodyparts.SelectBodyPartsActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.android.gms.ads.MobileAds
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint

//TODO ボタン文言の変更とかViewModelでやる
@AndroidEntryPoint
class StartActivity : AppCompatActivity(), StartNavigator {

    private lateinit var binding: ActivityStartBinding

    private val startViewModel by viewModels<StartViewModel>()

    private val signInLauncher =
        registerForActivityResult(FirebaseAuthUIActivityResultContract()) { result ->
            this.onSignInResult(result)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //AdMobの初期化
        MobileAds.initialize(this)
        if (AppSettings.isCompleteTutorial(this)) {
            finish()
            startActivity(HomeActivity.createIntent(this))
        }

        AppSettings.createUuidIfNeed(this)

        supportActionBar?.hide()
        startViewModel.navigator = this
        binding = DataBindingUtil.setContentView(this, R.layout.activity_start)
        binding.run {
            pager.adapter = PagerAdapter(this@StartActivity)
            withoutSignInButton.setOnClickListener {
                finish()
                startActivity(SelectBodyPartsActivity.createIntent(this@StartActivity))
            }
            nextButton.setOnClickListener {
                pager.currentItem = if (pager.currentItem != 2) {
                    pager.currentItem + 1
                } else {
                    pager.currentItem
                }
            }
            pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if (position == 2) {
                        nextButton.text = getString(R.string.first_boot_third_sign_in)
                        nextButton.setOnClickListener {
                            val providers = arrayListOf(AuthUI.IdpConfig.GoogleBuilder().build())
                            val signInIntent = AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setAvailableProviders(providers)
                                .build()
                            signInLauncher.launch(signInIntent)
                        }
                        withoutSignInButton.visibility = View.VISIBLE
                        termsDescription.visibility = View.VISIBLE
                    } else {
                        nextButton.text = getString(R.string.next)
                        withoutSignInButton.visibility = View.GONE
                        termsDescription.visibility = View.GONE
                    }
                }
            })
        }
        binding.indicator.setViewPager(binding.pager)
    }

    private fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
        val user = FirebaseAuth.getInstance().currentUser
        if (result.resultCode == RESULT_OK && user != null) {
            startViewModel.createUser(AppSettings.getUuid(this), user.uid)
        } else {
            Toast.makeText(
                this,
                getText(R.string.first_boot_third_sign_in_fail),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private inner class PagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = INTRODUCTION_PAGE_NUM
        override fun createFragment(position: Int): Fragment =
            when (position) {
                0 -> FirstIntroductionFragment.newInstance()
                1 -> SecondIntroductionFragment.newInstance()
                2 -> ThirdIntroductionFragment.newInstance()
                else -> FirstIntroductionFragment.newInstance()
            }
    }

    companion object {
        const val INTRODUCTION_PAGE_NUM = 3
    }

    override fun showNextPage() {
        finish()
        startActivity(SelectBodyPartsActivity.createIntent(this))
        Toast.makeText(
            this,
            getText(R.string.first_boot_third_sign_in_success),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun showErrorToast() {
        Toast.makeText(
            this,
            getString(R.string.network_fail),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun signOutIfNeed() {
        //FirebaseAuthだけ成功して通信に失敗すると、アプリ側とサーバー側で認識してるログイン状態に齟齬が生まれるから、通信失敗したらログアウト
        if (FirebaseAuth.getInstance().currentUser != null) {
            AuthUI.getInstance().signOut(this)
        }
    }
}