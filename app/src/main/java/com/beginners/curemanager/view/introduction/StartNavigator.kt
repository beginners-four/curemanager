package com.beginners.curemanager.view.introduction

interface StartNavigator {
    fun showNextPage()
    fun showErrorToast()
    fun signOutIfNeed()
}