package com.beginners.curemanager.view.introduction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beginners.curemanager.api.CommonRetrofitService
import com.beginners.curemanager.usecase.StartUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StartViewModel @Inject constructor(val useCase: StartUseCase) : ViewModel() {
    var navigator: StartNavigator? = null

    private val _state = MutableLiveData(CommonRetrofitService.State.LOADING)
    val state: LiveData<CommonRetrofitService.State> = _state

    private val exceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, _ ->
            _state.postValue(CommonRetrofitService.State.FAILED)
            navigator?.showErrorToast()
            navigator?.signOutIfNeed()
        }

    fun createUser(uuid: String, uid: String) {
        viewModelScope.launch(exceptionHandler) {
            useCase.createUser(uuid, uid)
            navigator?.showNextPage()
        }
    }
}