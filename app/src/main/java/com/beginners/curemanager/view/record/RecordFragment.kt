package com.beginners.curemanager.view.record

import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsParamBuilder
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentRecordBinding
import com.beginners.curemanager.utils.DateFormat
import com.beginners.curemanager.view.record.calendar.CalendarDayBinder
import com.beginners.curemanager.view.record.calendar.CalendarHeaderBinder
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import dagger.hilt.android.AndroidEntryPoint
import java.time.DayOfWeek
import java.time.YearMonth
import java.util.*

@AndroidEntryPoint
class RecordFragment : Fragment() {

    private var _binding: FragmentRecordBinding? = null
    private val binding get() = _binding!!

    private val recordViewModel by viewModels<RecordViewModel>()

    private lateinit var weekStart: Calendar
    private lateinit var weekEnd: Calendar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecordBinding.inflate(inflater, container, false)

        binding.run {
            lifecycleOwner = viewLifecycleOwner
            viewModel = recordViewModel.apply {
                conditionRecordListData.observe(viewLifecycleOwner) {
                    //最初に表示する週の設定
                    changePeriodBarChart(weekStart, weekEnd)
                }
            }
            recordExerciseCalendarView.dayBinder = CalendarDayBinder(recordViewModel)
            recordExerciseCalendarView.monthHeaderBinder = CalendarHeaderBinder(binding.recordExerciseCalendarView)
            //TODO 実際の初回リリース月になおす
            val startMonth = YearMonth.of(2022,8)
            val currentMonth = YearMonth.now()
            recordExerciseCalendarView.setup(startMonth, currentMonth, DayOfWeek.MONDAY)
            recordExerciseCalendarView.scrollToMonth(currentMonth)
        }
        setUpBarChart()

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.RECORD,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.ACTION,
                FirebaseEvent.Value.show).build()
        )
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        recordViewModel.fetchRecordData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUpBarChart() {
        val barChart = binding.recordConditionBarChart.apply {
            description.isEnabled = false
            legend.isEnabled = false
        }

        //Y軸(左)
        barChart.axisLeft.apply {
            axisMinimum = 0f
            axisMaximum = 5f
            labelCount = 5
            setDrawTopYLabelEntry(true)
        }

        //Y軸(右)
        barChart.axisRight.apply {
            setDrawLabels(false)
            setDrawGridLines(false)
            setDrawZeroLine(false)
            setDrawTopYLabelEntry(false)
        }

        //X軸に表示するLabelのリスト(最初の""は原点の位置)
        val labels =
            listOf(
                "",
                requireContext().getString(R.string.record_monday),
                requireContext().getString(R.string.record_tuesday),
                requireContext().getString(R.string.record_wednesday),
                requireContext().getString(R.string.record_thursday),
                requireContext().getString(R.string.record_friday),
                requireContext().getString(R.string.record_saturday),
                requireContext().getString(R.string.record_sunday)
            )

        //X軸
        barChart.xAxis.apply {
            isEnabled = true
            textColor = Color.BLACK
            valueFormatter = IndexAxisValueFormatter(labels)
            position = XAxis.XAxisPosition.TOP
            setDrawLabels(true)
            setDrawGridLines(false)
        }

        when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
            Calendar.MONDAY -> {
                weekStart = Calendar.getInstance()
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 6) }
            }
            Calendar.TUESDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -1) }
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 5) }
            }
            Calendar.WEDNESDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -2) }
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 4) }
            }
            Calendar.THURSDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -3) }
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 3) }
            }
            Calendar.FRIDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -4) }
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 2) }
            }
            Calendar.SATURDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -5) }
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 1) }
            }
            Calendar.SUNDAY -> {
                weekStart = Calendar.getInstance().apply { add(Calendar.DATE, -6) }
                weekEnd = Calendar.getInstance()
            }
            else -> {
                weekStart = Calendar.getInstance()
                weekEnd = Calendar.getInstance().apply { add(Calendar.DATE, 5) }
            }
        }
        binding.recordConditionWeekLeftArrow.setOnClickListener {
            weekStart = weekStart.apply { add(Calendar.DATE, -7) }
            weekEnd = weekEnd.apply { add(Calendar.DATE, -7) }
            changePeriodBarChart(weekStart, weekEnd)
        }
        binding.recordConditionWeekRightArrow.setOnClickListener {
            weekStart = weekStart.apply { add(Calendar.DATE, 7) }
            weekEnd = weekEnd.apply { add(Calendar.DATE, 7) }
            changePeriodBarChart(weekStart, weekEnd)
        }
    }

    private fun changePeriodBarChart(startDate: Calendar, endDate: Calendar) {
        binding.recordConditionWeek.text = requireContext().getString(
            R.string.record_condition_day_between,
            DateFormat.yyyyMMdd_slash.calToString(startDate),
            DateFormat.yyyyMMdd_slash.calToString(endDate)
        )
        binding.recordConditionBarChart.run {
            data = BarData(getBarData(resources, startDate))
            animateY(1200, Easing.EaseInOutQuad)
            invalidate()
        }
    }

    //棒グラフのデータを取得
    private fun getBarData(resources: Resources, startDate: Calendar): List<IBarDataSet> {

        fun findCondition(targetDate: Calendar): Float {
            recordViewModel.conditionRecordListData.value?.forEach {
                val date = DateFormat.yyyyMMdd_hyphen.stringToCal(it.date) ?: return@forEach
                if (DateFormat.isSameDate(targetDate, date)) {
                    return it.condition
                }
            }
            return 0f
        }

        val x = listOf(1f, 2f, 3f, 4f, 5f, 6f, 7f)//X軸データ
        val y = mutableListOf(0f, 0f, 0f, 0f, 0f, 0f, 0f)
        (0..6).forEach {
            val targetDate = Calendar.getInstance()
            targetDate.time = startDate.time
            targetDate.add(Calendar.DAY_OF_YEAR, it)
            y[it] = findCondition(targetDate)
        }

        val entryList = mutableListOf<BarEntry>()
        for (i in x.indices) {
            entryList.add(BarEntry(x[i], y[i]))
        }
        val dataSet = BarDataSet(entryList, "").apply {
            setDrawValues(false)
            isHighlightEnabled = false
            //Barの色をセット
            val startColor =
                ResourcesCompat.getColor(resources, R.color.record_color_bar_chart_start, null)
            val endColor =
                ResourcesCompat.getColor(resources, R.color.record_color_bar_chart_end, null)
            setGradientColor(startColor, endColor)
        }

        return mutableListOf<IBarDataSet>().apply { add(dataSet) }
    }
}