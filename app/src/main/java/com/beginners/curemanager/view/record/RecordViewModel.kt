package com.beginners.curemanager.view.record

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beginners.curemanager.api.CommonRetrofitService
import com.beginners.curemanager.api.ConditionRecord
import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.usecase.RecordUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecordViewModel @Inject constructor(val useCase: RecordUseCase) : ViewModel() {

    private val _fetchState = MutableLiveData(CommonRetrofitService.State.LOADING)
    val fetchState: LiveData<CommonRetrofitService.State> = _fetchState

    private val _totalExerciseDays = MutableLiveData(0)
    val totalExerciseDays: LiveData<Int> = _totalExerciseDays

    private val _exerciseRecordListData = MutableLiveData<List<ExerciseRecordData>>()
    val exerciseRecordListData: LiveData<List<ExerciseRecordData>> = _exerciseRecordListData

    private val _conditionRecordListData = MutableLiveData<List<ConditionRecord>>()
    val conditionRecordListData: LiveData<List<ConditionRecord>> = _conditionRecordListData

    private val exceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, _ ->
            _fetchState.postValue(CommonRetrofitService.State.FAILED)
        }

    fun fetchRecordData() {
        viewModelScope.launch(exceptionHandler) {
            _fetchState.postValue(CommonRetrofitService.State.LOADING)
            val apiAsyncList = listOf(
                async {
                    _totalExerciseDays.postValue(useCase.getTotalExerciseDays().body()?.data?.day)
                },
                async {
                    _exerciseRecordListData.postValue(useCase.getExerciseRecord().body()?.data)
                },
                async {
                    _conditionRecordListData.postValue(useCase.getConditionsRecord().body()?.data)
                }
            )
            apiAsyncList.awaitAll()
            _fetchState.postValue(CommonRetrofitService.State.SUCCESS)
        }
    }
}