package com.beginners.curemanager.view.record.calendar

import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.beginners.curemanager.R
import com.beginners.curemanager.model.ExerciseMainType
import com.beginners.curemanager.model.ExerciseRecordData
import com.beginners.curemanager.utils.DateFormat
import com.beginners.curemanager.view.record.RecordViewModel
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder

class CalendarDayBinder(val viewModel: RecordViewModel) : DayBinder<CalendarDayContainer> {
    override fun create(view: View): CalendarDayContainer = CalendarDayContainer(view)

    override fun bind(container: CalendarDayContainer, day: CalendarDay) {
        if (day.owner != DayOwner.THIS_MONTH) {
            // 月頭前と月終わりあとの日付を見えなくする
            container.binding.calendarDay.visibility = View.INVISIBLE
        }

        val dayRecord = mutableListOf<ExerciseRecordData>()
        var containAcuPoint = false
        var containStretch = false
        var containAerobic = false
        var containWorkOut = false
        viewModel.exerciseRecordListData.value?.forEach {
            val recordDay = DateFormat.ISO_8601.stringToLocaleDate(it.startTime ?: return@forEach)
            if (recordDay == day.date) {
                dayRecord.add(it)
                if (it.mainType == ExerciseMainType.ACUPOINT.name) containAcuPoint = true
                if (it.mainType == ExerciseMainType.STRETCH.name) containStretch = true
                if (it.mainType == ExerciseMainType.AEROBIC.name) containAerobic = true
                if (it.mainType == ExerciseMainType.STRENGTH.name) containWorkOut = true
            }
        }

        container.binding.calendarDayText.text = day.day.toString()
        if (containAcuPoint) container.binding.tsuboDot.visibility = View.VISIBLE
        if (containStretch) container.binding.stretchDot.visibility = View.VISIBLE
        if (containAerobic) container.binding.aerobicsDot.visibility = View.VISIBLE
        if (containWorkOut) container.binding.workOutDot.visibility = View.VISIBLE


        if (containAcuPoint || containStretch || containAerobic || containWorkOut) {
            container.binding.calendarDayText.background = ResourcesCompat.getDrawable(
                container.view.resources,
                R.drawable.corner_radius_bg_yellow,
                null
            )
            container.binding.calendarDayText.setTextColor(
                ResourcesCompat.getColor(
                    container.view.resources,
                    R.color.white,
                    null
                )
            )
        }
    }
}