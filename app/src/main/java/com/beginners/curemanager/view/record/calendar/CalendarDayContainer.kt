package com.beginners.curemanager.view.record.calendar

import android.view.View
import com.beginners.curemanager.databinding.CalendarDayLayoutBinding
import com.kizitonwose.calendarview.ui.ViewContainer

class CalendarDayContainer(view: View) : ViewContainer(view) {
    val binding = CalendarDayLayoutBinding.bind(view)
}