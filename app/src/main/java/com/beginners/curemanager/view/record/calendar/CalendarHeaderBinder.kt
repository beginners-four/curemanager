package com.beginners.curemanager.view.record.calendar

import android.view.View
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.utils.next
import com.kizitonwose.calendarview.utils.previous

class CalendarHeaderBinder(val calendar: com.kizitonwose.calendarview.CalendarView) :
    MonthHeaderFooterBinder<CalendarHeaderContainer> {
    override fun create(view: View) = CalendarHeaderContainer(view)
    override fun bind(
        container: CalendarHeaderContainer,
        month: CalendarMonth
    ) {
        container.binding.run {
            monthText.text = month.yearMonth.toString()
            leftArrow.setOnClickListener {
                calendar.scrollToMonth(month.yearMonth.previous)
            }
            rightArrow.setOnClickListener {
                calendar.scrollToMonth(month.yearMonth.next)
            }
        }
    }
}