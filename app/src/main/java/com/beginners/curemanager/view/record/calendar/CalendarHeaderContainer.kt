package com.beginners.curemanager.view.record.calendar

import android.view.View
import com.beginners.curemanager.databinding.CalendarHeaderBinding
import com.kizitonwose.calendarview.ui.ViewContainer

class CalendarHeaderContainer(view: View) : ViewContainer(view) {
    val binding = CalendarHeaderBinding.bind(view)
}