package com.beginners.curemanager.view.selectbodyparts

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.beginners.curemanager.R
import com.beginners.curemanager.databinding.ActivitySelectBodyPartsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectBodyPartsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySelectBodyPartsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_body_parts)
        supportFragmentManager.beginTransaction().run {
            replace(R.id.container, SelectCurePointFragment.newInstance(true))
            commit()
        }
    }

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, SelectBodyPartsActivity::class.java)
        }
    }
}