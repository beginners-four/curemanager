package com.beginners.curemanager.view.selectbodyparts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentSelectBodyPartsBinding
import com.beginners.curemanager.model.CurePoint
import com.beginners.curemanager.view.home.HomeViewModel
import com.beginners.curemanager.view.setting.SettingReminderFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectCurePointFragment : Fragment(), SelectCurePointNavigator {

    private var _binding: FragmentSelectBodyPartsBinding? = null
    private val binding get() = _binding!!

    private val selectCurePointViewModel by viewModels<SelectCurePointViewModel>()
    private val homeViewModel by activityViewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val isInitialSetUp = arguments?.getBoolean(BUNDLE_KEY_IS_INITIAL_SET_UP)
        _binding = FragmentSelectBodyPartsBinding.inflate(inflater, container, false)
        selectCurePointViewModel.navigator = this
        if (isInitialSetUp == true) {
            (activity as? SelectBodyPartsActivity)?.supportActionBar?.hide()
            selectCurePointViewModel.setIsInitialSetUp()
        }
        binding.lifecycleOwner = this
        binding.viewModel = selectCurePointViewModel
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun saveCurePoint() {
        val curePointList = mutableListOf<CurePoint>()
        binding.run {
            if (elbow.isChecked) curePointList.add(CurePoint.ELBOW)
            if (knees.isChecked) curePointList.add(CurePoint.KNEES)
            if (backache.isChecked) curePointList.add(CurePoint.BACKACHE)
            if (headache.isChecked) curePointList.add(CurePoint.HEADACHE)
            if (stiffnessShoulder.isChecked) curePointList.add(CurePoint.SHOULDER)
            if (stiffnessBack.isChecked) curePointList.add(CurePoint.STIFFNESS_BACK)
            if (asthenopia.isChecked) curePointList.add(CurePoint.ASTHENOPIA)
            if (feebleness.isChecked) curePointList.add(CurePoint.FEEBLENESS)
        }
        curePointList.add(CurePoint.ADD_EXERCISE)
        homeViewModel.setCurePointList(curePointList)

        Toast.makeText(
            requireContext(),
            getString(R.string.save_complete),
            Toast.LENGTH_SHORT
        ).show()

        FirebaseAnalyticsUtils.sendEvent(requireContext(), FirebaseEvent.Name.SELECT_CURE_POINT)
    }

    override fun showNextPage() {
        parentFragmentManager.beginTransaction().run {
            add(R.id.container, SettingReminderFragment.newInstance(true))
            commit()
        }
    }

    override fun backPage() {
        findNavController().popBackStack()
    }

    companion object {
        private const val BUNDLE_KEY_IS_INITIAL_SET_UP = "bundle_key_is_initial_set_up"
        fun newInstance(isInitialSetUp: Boolean) = SelectCurePointFragment().apply {
            val bundle = Bundle().apply {
                putBoolean(BUNDLE_KEY_IS_INITIAL_SET_UP, isInitialSetUp)
            }
            arguments = bundle
        }
    }
}