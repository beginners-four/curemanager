package com.beginners.curemanager.view.selectbodyparts

interface SelectCurePointNavigator {
    fun saveCurePoint()
    fun showNextPage()
    fun backPage()
}