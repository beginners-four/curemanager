package com.beginners.curemanager.view.selectbodyparts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beginners.curemanager.model.CurePoint
import com.beginners.curemanager.usecase.CurePointListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SelectCurePointViewModel @Inject constructor(useCase: CurePointListUseCase) :
    ViewModel() {
    var navigator: SelectCurePointNavigator? = null

    private val _isInitialSetUp = MutableLiveData<Boolean>()
    val isInitialSetUp: LiveData<Boolean>
        get() = _isInitialSetUp

    private val _curePointList = MutableLiveData(useCase.curePointList.map { it.curePointData.first }.toMutableList())
    val curePointList: LiveData<MutableList<CurePoint>>
        get() = _curePointList

    fun setIsInitialSetUp() {
        _isInitialSetUp.value = true
    }

    fun onClickCurePointButton(curePoint: CurePoint) {
        if (_curePointList.value?.contains(curePoint) == true) {
            _curePointList.value = _curePointList.value?.apply { remove(curePoint) }
        } else {
            _curePointList.value = _curePointList.value?.apply { add(curePoint) }
        }
    }

    fun onClickSaveButton() {
        navigator?.saveCurePoint()
        if (_isInitialSetUp.value == true) {
            navigator?.showNextPage()
        } else {
            navigator?.backPage()
        }
    }
}