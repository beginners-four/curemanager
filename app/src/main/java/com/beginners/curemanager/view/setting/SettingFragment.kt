package com.beginners.curemanager.view.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsParamBuilder
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.analytics.FirebaseEvent
import com.beginners.curemanager.databinding.FragmentSettingBinding
import com.beginners.curemanager.utils.AppSettings
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingFragment : Fragment(), SettingNavigator {

    private var _binding: FragmentSettingBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<SettingViewModel>()

    private val signInLauncher =
        registerForActivityResult(FirebaseAuthUIActivityResultContract()) { result ->
            this.onSignInResult(result)
        }

    private fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
        val user = FirebaseAuth.getInstance().currentUser
        if (result.resultCode == AppCompatActivity.RESULT_OK && user != null) {
            viewModel.handoverUser(AppSettings.getUuid(requireContext()), user.uid)
        } else {
            Toast.makeText(
                requireContext(),
                getText(R.string.first_boot_third_sign_in_fail),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel.apply {
            navigator = this@SettingFragment
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showReminderSetting() {
        val navHostFragment =
            requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_home) as NavHostFragment
        val navController = navHostFragment.navController
        val action = SettingFragmentDirections.actionNavigationSettingsToSettingReminderFragment()
        navController.navigate(action)

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.reminder).build()
        )
    }

    override fun onClickInquiry() {
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.settings_inquiry_title))
            .setMessage(getString(R.string.settings_inquiry_mail_address))
            .setPositiveButton(getString(R.string.close), null)
            .show()

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.inquiry).build()
        )
    }

    override fun onClickAbout() {
        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.about).build()
        )
    }

    override fun onClickTerm() {
        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.term).build()
        )
    }

    override fun onClickPolicy() {
        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.policy).build()
        )
    }

    override fun showSignInDialog() {
        val title = requireContext().getString(R.string.settings_logout_dialog_title)
        val message = requireContext().getString(R.string.settings_sign_in_dialog_message)
        val positiveButtonText = requireContext().getString(R.string.yes)
        val negativeButtonText = requireContext().getString(R.string.no)
        val dialog = MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(positiveButtonText) { _, _ ->
                val providers = arrayListOf(AuthUI.IdpConfig.GoogleBuilder().build())
                val signInIntent = AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build()
                signInLauncher.launch(signInIntent)
            }
            setNegativeButton(negativeButtonText) { _, _ -> }
        }.create()
        dialog.show()

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.sigin_in).build()
        )
    }

    override fun showSignOutDialog() {
        val title = requireContext().getString(R.string.settings_sign_out)
        val message = requireContext().getString(R.string.settings_sign_out_dialog_message)
        val positiveButtonText = requireContext().getString(R.string.yes)
        val negativeButtonText = requireContext().getString(R.string.no)
        val dialog = MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(positiveButtonText) { _, _ ->
                AuthUI.getInstance()
                    .signOut(requireContext())
                    .addOnCompleteListener {
                        viewModel.setIsCompleteSignIn(false)
                        Toast.makeText(
                            requireContext(),
                            getText(R.string.settings_sign_out_toast),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }
            setNegativeButton(negativeButtonText) { _, _ -> }
        }.create()
        dialog.show()

        FirebaseAnalyticsUtils.sendEvent(
            requireContext(),
            FirebaseEvent.Name.SETTING,
            FirebaseAnalyticsParamBuilder().putParam(
                FirebaseEvent.Key.TITLE,
                FirebaseEvent.Value.sigin_out).build()
        )
    }

    override fun showSignInToast() {
        Toast.makeText(
            requireContext(),
            getText(R.string.settings_sign_in_toast),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun showErrorToast() {
        Toast.makeText(
            requireContext(),
            getString(R.string.network_fail),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun signOutIfNeed() {
        //FirebaseAuthだけ成功して通信に失敗すると、アプリ側とサーバー側で認識してるログイン状態に齟齬が生まれるから、通信失敗したらログアウト
        if (FirebaseAuth.getInstance().currentUser != null) {
            AuthUI.getInstance().signOut(requireContext())
        }
    }
}