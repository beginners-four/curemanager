package com.beginners.curemanager.view.setting

interface SettingNavigator {
    fun showReminderSetting()
    fun onClickInquiry()
    fun onClickAbout()
    fun onClickTerm()
    fun onClickPolicy()
    fun showSignInDialog()
    fun showSignOutDialog()
    fun showSignInToast()
    fun showErrorToast()
    fun signOutIfNeed()
}