package com.beginners.curemanager.view.setting

import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.work.WorkManager
import com.beginners.curemanager.R
import com.beginners.curemanager.analytics.FirebaseAnalyticsUtils
import com.beginners.curemanager.databinding.FragmentSettingReminderBinding
import com.beginners.curemanager.notifications.ExerciseReminderWorker
import com.beginners.curemanager.notifications.ExerciseReminderWorker.Companion.TAG_EXERCISE_REMINDER_WORKER
import com.beginners.curemanager.view.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class SettingReminderFragment : Fragment(), SettingReminderNavigator {
    private var _binding: FragmentSettingReminderBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<SettingReminderViewModel>()
    private lateinit var callback: OnBackPressedCallback

    private lateinit var manager: WorkManager

    //TimePicker表示→時刻選択→再表示したときの初期表示が選択した時刻になるようにするための一時的に保持する時刻
    private var pickedHour: Int? = null
    private var pickedMinute: Int? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = requireActivity().onBackPressedDispatcher.addCallback {
            backPage()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        manager = WorkManager.getInstance(requireContext())
        pickedHour = viewModel.remindTime.value?.get(Calendar.HOUR_OF_DAY)
        pickedMinute = viewModel.remindTime.value?.get(Calendar.MINUTE)
        val isInitialSetUp = arguments?.getBoolean(BUNDLE_KEY_IS_INITIAL_SET_UP)
        _binding = FragmentSettingReminderBinding.inflate(inflater, container, false)
        viewModel.navigator = this
        if (isInitialSetUp == true) {
            viewModel.setIsInitialSetUp()
        }
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        callback.remove()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                backPage()
                false
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showTimePicker() {
        val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            viewModel.setTime(hour, minute)
            pickedHour = hour
            pickedMinute = minute
        }
        val cal = Calendar.getInstance()
        val hour = if (pickedHour != null) {
            pickedHour as Int
        } else {
            cal.get(Calendar.HOUR_OF_DAY)
        }
        val minute = if (pickedMinute != null) {
            pickedMinute as Int
        } else {
            cal.get(Calendar.MINUTE)
        }
        TimePickerDialog(
            requireContext(),
            timeSetListener,
            hour,
            minute,
            true
        ).show()
    }

    override fun setAlarm() {
        //一旦キャンセルしてからでないと、何個でもアラームセットできてしまうため
        manager.cancelAllWorkByTag(TAG_EXERCISE_REMINDER_WORKER)
        manager.enqueue(ExerciseReminderWorker.createOneTimeRequest(requireContext()))

        FirebaseAnalyticsUtils.sendUserPropertyExerciseReminder(requireContext(),true)
    }

    override fun cancelAlarm() {
        manager.cancelAllWorkByTag(TAG_EXERCISE_REMINDER_WORKER)

        FirebaseAnalyticsUtils.sendUserPropertyExerciseReminder(requireContext(),false)
    }

    override fun showNextPage() {
        activity?.finish()
        startActivity(HomeActivity.createIntent(requireContext()))
    }

    override fun showSettingOnToast() {
        Toast.makeText(
            requireContext(),
            getString(R.string.setting_reminder_on),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun backPage() {
        //設定画面から戻るときに設定保存する
        viewModel.saveSetting()
        val toastText = if (viewModel.isReminderOn.value == true) {
            getString(R.string.setting_reminder_on)
        } else {
            getString(R.string.setting_reminder_off)
        }
        Toast.makeText(
            requireContext(),
            toastText,
            Toast.LENGTH_SHORT
        ).show()
        findNavController().popBackStack()
    }

    companion object {
        private const val BUNDLE_KEY_IS_INITIAL_SET_UP = "bundle_key_is_initial_set_up"

        fun newInstance(isInitialSetUp: Boolean) = SettingReminderFragment().apply {
            val bundle = Bundle().apply {
                putBoolean(BUNDLE_KEY_IS_INITIAL_SET_UP, isInitialSetUp)
            }
            arguments = bundle
        }
    }
}