package com.beginners.curemanager.view.setting

interface SettingReminderNavigator {
    fun showTimePicker()
    fun setAlarm()
    fun cancelAlarm()
    fun showNextPage()
    fun showSettingOnToast()
}