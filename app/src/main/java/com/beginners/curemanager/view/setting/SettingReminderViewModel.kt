package com.beginners.curemanager.view.setting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beginners.curemanager.usecase.SettingReminderUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SettingReminderViewModel @Inject constructor(private val useCase: SettingReminderUseCase) :
    ViewModel() {
    var navigator: SettingReminderNavigator? = null

    private val _isInitialSetUp = MutableLiveData<Boolean>()
    val isInitialSetUp: LiveData<Boolean>
        get() = _isInitialSetUp

    private val _remindTime = MutableLiveData(
        Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, useCase.remindHour)
            set(Calendar.MINUTE, useCase.remindMinute)
        }
    )
    val remindTime: LiveData<Calendar>
        get() = _remindTime

    private val _isReminderOn = MutableLiveData(useCase.isSettingOn)
    val isReminderOn: LiveData<Boolean>
        get() = _isReminderOn

    fun setIsInitialSetUp() {
        _isInitialSetUp.value = true
    }

    fun onClickTime() {
        navigator?.showTimePicker()
    }

    fun setTime(hour: Int, minute: Int) {
        _remindTime.value = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minute)
        }
    }

    fun onClickSwitch() {
        _isReminderOn.value = _isReminderOn.value?.not()
    }

    fun saveSetting() {
        //リマインダーON/OFFの保存
        useCase.isSettingOn = _isReminderOn.value == true

        //リマインド時刻の保存
        val hour = _remindTime.value?.get(Calendar.HOUR_OF_DAY)
        val minute = _remindTime.value?.get(Calendar.MINUTE)
        if (hour != null && minute != null) {
            useCase.setRemindTIme(hour, minute)
        }

        //アラームセット
        if (_isReminderOn.value == true) {
            navigator?.setAlarm()
        } else {
            navigator?.cancelAlarm()
        }
    }

    //初回起動時の設定保存用
    fun onClickSettingSave() {
        saveSetting()
        if (_isReminderOn.value == true) {
            navigator?.showSettingOnToast()
        }
        navigator?.showNextPage()
    }

    fun onClickSkip() {
        useCase.isSettingOn = false
        navigator?.showNextPage()
    }
}