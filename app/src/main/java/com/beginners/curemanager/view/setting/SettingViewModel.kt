package com.beginners.curemanager.view.setting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beginners.curemanager.usecase.SettingUseCase
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingViewModel @Inject constructor(val useCase: SettingUseCase) : ViewModel() {

    private val _isCompleteSignIn = MutableLiveData(FirebaseAuth.getInstance().currentUser != null)
    val isCompleteSignIn: LiveData<Boolean>
        get() = _isCompleteSignIn

    var navigator: SettingNavigator? = null

    private val exceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, _ ->
            navigator?.showErrorToast()
            navigator?.signOutIfNeed()
        }

    fun onClickSettingReminder() {
        navigator?.showReminderSetting()
    }

    fun onClickInquiry(){
        navigator?.onClickInquiry()
    }

    fun onClickAbout(){
        navigator?.onClickAbout()
    }

    fun onClickTerm(){
        navigator?.onClickTerm()
    }

    fun onClickPolicy(){
        navigator?.onClickPolicy()
    }

    fun setIsCompleteSignIn(isCompleteSignIn: Boolean) {
        _isCompleteSignIn.value = isCompleteSignIn
    }

    fun onClickSignInManager() {
        if (_isCompleteSignIn.value == true) {
            navigator?.showSignOutDialog()
        } else {
            navigator?.showSignInDialog()
        }
    }

    fun handoverUser(uuid: String, uid: String) {
        viewModelScope.launch(exceptionHandler) {
            useCase.handoverUser(uuid, uid)
            _isCompleteSignIn.value = true
            navigator?.showSignInToast()
        }
    }
}