<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="com.beginners.curemanager.utils.DateFormat" />

        <variable
            name="viewModel"
            type="com.beginners.curemanager.view.setting.SettingReminderViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/app_bg">

        <ScrollView
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:fillViewport="true"
            app:layout_constraintBottom_toTopOf="@id/next_button"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <TextView
                    android:id="@+id/setting_reminder_title"
                    style="@style/Title2"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_xxxlarge"
                    android:layout_marginTop="40dp"
                    android:layout_marginEnd="@dimen/space_xxxlarge"
                    android:text="@string/setting_reminder_title"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/setting_reminder_description"
                    style="@style/Title3"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_xxxlarge"
                    android:layout_marginTop="40dp"
                    android:layout_marginEnd="@dimen/space_xxxlarge"
                    android:text="@string/setting_reminder_description"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/setting_reminder_title" />

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/setting_reminder"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_xxxlarge"
                    android:layout_marginTop="@dimen/space_normal"
                    android:layout_marginEnd="@dimen/space_xxxlarge"
                    android:background="@drawable/corner_radius_bg_white"
                    android:paddingStart="@dimen/space_xxlarge"
                    android:paddingTop="@dimen/space_xlarge"
                    android:paddingEnd="@dimen/space_xxlarge"
                    android:paddingBottom="@dimen/space_xlarge"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/setting_reminder_description">

                    <TextView
                        android:id="@+id/setting_reminder_time"
                        style="@style/Title2Bold"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:background="?attr/selectableItemBackground"
                        android:onClick="@{() -> viewModel.onClickTime()}"
                        android:text="@{DateFormat.HHmm.calToString(viewModel.remindTime)}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent"
                        tools:text="08:00" />

                    <androidx.appcompat.widget.SwitchCompat
                        android:id="@+id/setting_reminder_switch"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:checked="@{viewModel.isReminderOn()}"
                        android:onClick="@{() -> viewModel.onClickSwitch()}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />
                </androidx.constraintlayout.widget.ConstraintLayout>

                <TextView
                    android:id="@+id/setting_reminder_tips_title"
                    style="@style/HighLightTitle"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_xxxlarge"
                    android:layout_marginTop="36dp"
                    android:text="@string/setting_reminder_tips_title"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/setting_reminder" />

                <TextView
                    android:id="@+id/setting_reminder_tips_description"
                    style="@style/Title3"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/space_large"
                    android:background="@drawable/corner_radius_bg_white"
                    android:gravity="center"
                    android:paddingStart="48dp"
                    android:paddingTop="@dimen/space_xxlarge"
                    android:paddingEnd="48dp"
                    android:paddingBottom="@dimen/space_xxlarge"
                    android:text="@string/setting_reminder_tips_description"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/setting_reminder_tips_title" />
            </androidx.constraintlayout.widget.ConstraintLayout>
        </ScrollView>

        <Button
            android:id="@+id/next_button"
            android:layout_width="@dimen/bottom_button_width"
            android:layout_height="@dimen/bottom_button_height"
            android:layout_marginTop="@dimen/space_large"
            android:onClick="@{() -> viewModel.onClickSettingSave()}"
            android:text="@string/next"
            android:visibility="@{viewModel.isInitialSetUp() ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toTopOf="@id/skip_button"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            tools:text="@string/next" />

        <TextView
            android:id="@+id/skip_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="@dimen/space_normal"
            android:onClick="@{() -> viewModel.onClickSkip()}"
            android:padding="@dimen/space_large"
            android:text="@string/skip"
            android:visibility="@{viewModel.isInitialSetUp() ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>